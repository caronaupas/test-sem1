function guardarEnSessionStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var txtValor = document.getElementById("txtValor"); /* Referencia al input de valor */
  var clave = txtClave.value;
  var valor = txtValor.value;
  sessionStorage.setItem(clave, valor);
  var objeto = {
    nombre:"Carol",
    apellidos:"Naupas Caraza",
    ciudad:"Lima",
    pais:"Perù"
  };
  sessionStorage.setItem("json", JSON.stringify(objeto));
}
function leerDeSessionStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var clave = txtClave.value;
  var valor = sessionStorage.getItem(clave);
  var spanValor = document.getElementById("spanValor");
  spanValor.innerText = valor;
  var datosUsuario = JSON.parse(sessionStorage.getItem("json"));
  console.log(datosUsuario.nombre);
  console.log(datosUsuario.pais);
  console.log(datosUsuario);
}
function removerDeSessionStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var clave = txtClave.value;
  var valor = sessionStorage.removeItem(clave);
  var spanValor = document.getElementById("spanValor");
  spanValor.innerText = "Elemento eliminado : " + clave;
}
function borrarSessionStorage() {
  sessionStorage.clear();
  var spanValor = document.getElementById("spanValor");
  spanValor.innerText = "Todos los elementos se eliminaron";
}
function lengthDeSessionStorage() {
  var numElements = sessionStorage.length;
  var spanValor = document.getElementById("spanValor");
  spanValor.innerText = "Nro. de Elementos en session es : " + numElements;
}
